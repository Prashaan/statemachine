# State Machine

Lightweight cross platform state machine implementation in Haxe

[Haxelib Release](https://lib.haxe.org/p/statemachine/) : Version 0.0.2

## Getting Started

Make sure that you currently have Haxe 3.x.x installed. Please watch this space for support for Haxe 4. 
```
haxelib install statemachine
```
## Usage

To create the State Machine:
````
var statemachine:IStateMachine = new StateMachine();
````

States now can easily be created using the `createState('statename')` function which can be chained to 
attach actions that will transition from the created state to the action mapped state. This can easily be followed from the example below
which depicts the states of matter. 

Callback function can be supplied as optional parameters when creating the state.

## Example

````
static public function main()
    {
        var stateMachine:IStateMachine = new StateMachine();

        stateMachine.createState("liquid", liquidEntered, liquidExited)
        .mapActionToState("vaporization", "gas")
        .mapActionToState("freeze", "solid")
        .setAsInitialState();

        stateMachine.createState("gas", gasEntered)
        .mapActionToState("condensation", "liquid")
        .mapActionToState("deposition", "solid");

        stateMachine.createState("solid")
        .mapActionToState("sublimation", "gas")
        .mapActionToState("melt", "liquid");

        Timer.delay(function()
        {
            stateMachine.doAction("freeze");
        }, 1000);

        Timer.delay(function()
        {
            stateMachine.doAction("sublimation");
        }, 2000);

        Timer.delay(function()
        {
            stateMachine.doAction("condensation");
            trace('Current state is : ${stateMachine.currentStateName}');
        }, 3000);
    }
````

`In which case the output is: Liquid`

## Built With

* [Tink](http://www.dropwizard.io/1.0.2/docs/) - Exposed the use signals using tink as an addition to adding callback functions
* [Munit](https://maven.apache.org/) - The unit testing framework used to ensure quality gates are in place across all platforms 

## Authors

* **Prashaan Pillay** 

See also the list of [contributors](https://gitlab.com/Prashaan/statemachine/graphs/master) who participated in this project.

## License

This project is licensed under the GPT License