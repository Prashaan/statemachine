import haxe.Timer;
import machine.IStateMachine;
import machine.StateMachine;

class Main
{
    static public function main()
    {
        var stateMachine:IStateMachine = new StateMachine();

        stateMachine.createState("liquid", liquidEntered, liquidExited)
        .mapActionToState("vaporization", "gas")
        .mapActionToState("freeze", "solid")
        .setAsInitialState();

        stateMachine.createState("gas", gasEntered)
        .mapActionToState("condensation", "liquid")
        .mapActionToState("deposition", "solid");

        stateMachine.createState("solid")
        .mapActionToState("sublimation", "gas")
        .mapActionToState("melt", "liquid");

        Timer.delay(function()
        {
            stateMachine.doAction("freeze");
        }, 1000);

        Timer.delay(function()
        {
            stateMachine.doAction("sublimation");
        }, 2000);

        Timer.delay(function()
        {
            stateMachine.doAction("condensation");
            trace('Current state is : ${stateMachine.currentStateName}');
        }, 3000);
    }

    static function liquidEntered()
    {
        trace("Liquid Entered: " + Std.int(Timer.stamp()));
    }

    static function liquidExited()
    {
        trace("Liquid Exited: " + Std.int(Timer.stamp()));
    }

    static function gasEntered()
    {
        trace("Gas Entered: " + Std.int(Timer.stamp()));
    }

    static function gasExited()
    {
        trace("Gas Exited: " + Std.int(Timer.stamp()));
    }
}
