package machine;

import state.IState;

interface IStateMachine
{
    var currentStateName(get, null):String;

    var currentState(get, null):IState;

    function createState(name:String, ?transitionIn:Void -> Void, ?transitionOut:Void -> Void):IStateMachine;

    function setAsInitialState():IStateMachine;

    function doAction(action:String):Void;

    function mapActionToState(action:String, state:String):IStateMachine;

    function attachCallbacks(transitionIn:Void -> Void, ?transitionOut:Void -> Void):Void;
}
