package machine;

import haxe.ds.StringMap;
import state.IState;
import state.State;

class StateMachine implements IStateMachine
{
    public var currentStateName(get, null):String;
    public var currentState(get, null):IState;

    @isVar var _currentState(default, set):State;
    var states:StringMap<State>;
    var currentlyCreatedState:State;

    public function new()
    {
        currentlyCreatedState = null;
        states = new StringMap<State>();
    }

    public function createState(name:String, ?transitionIn:Void -> Void, ?transitionOut:Void -> Void):IStateMachine
    {
        if (name == null) return null;

        if (states.exists(name))
        {
            currentlyCreatedState = states.get(name);
            attachCallbacks(transitionIn, transitionOut);
        }
        else
        {
            currentlyCreatedState = new State(name);
            attachCallbacks(transitionIn, transitionOut);

            if (_currentState == null)
            {
                setAsInitialState();
            }

            states.set(currentlyCreatedState.name, currentlyCreatedState);
        }
        return this;
    }

    public function setAsInitialState():IStateMachine
    {
        _currentState = currentlyCreatedState;

        return this;
    }

    public function attachCallbacks(transitionIn:Void -> Void, ?transitionOut:Void -> Void)
    {
        currentlyCreatedState.transitionInCallback = transitionIn;
        currentlyCreatedState.transitionOutCallback = transitionOut;
    }

    public function doAction(action:String)
    {
        if (_currentState == null) return;

        _currentState.transitionOut(action);
        _currentState = cast _currentState.transitioningTo(action);
    }

    public function mapActionToState(actionName:String, stateName:String):IStateMachine
    {
        if (currentlyCreatedState == null || actionName == null || stateName == null) return null;

        var neededState:State = null;

        if (states.exists(stateName))
        {
            neededState = states.get(stateName);
        }
        else
        {
            neededState = new State(stateName);
            states.set(neededState.name, neededState);
        }

        currentlyCreatedState.mapActionToState(actionName, neededState);
        return this;
    }

    function get_currentStateName():String
    {
        if (_currentState == null) return null;
        return _currentState.name;
    }

    function set__currentState(state:State):State
    {
        _currentState = state;

        _currentState.transitionIn();

        return _currentState;
    }

    function get_currentState():IState
    {
        return _currentState;
    }
}
