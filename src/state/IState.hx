package state;

import tink.CoreApi.Signal;

interface IState
{
    var name(default, null):String;
    var isActive(default, null):Bool;

    var onStateEntered(default, null):Signal<Void -> Void>;
    var onStateExited(default, null):Signal<Void -> Void>;

    var previousState(default, null):IState;

    function mapActionToState(action:String, state:IState):Void;
}
