package state;

import haxe.ds.StringMap;
import tink.core.Signal;
import tink.CoreApi.Signal;

class State implements IState
{
    public var name(default, null):String;
    public var previousState(default, null):IState;
    public var isActive(default, null):Bool;

    public var transitionInCallback:Void -> Void;
    public var transitionOutCallback:Void -> Void;

    public var onStateEntered:Signal<Void -> Void>;
    var _onStateEntered:SignalTrigger<Void -> Void>;
    public var onStateExited:Signal<Void -> Void>;
    var _onStateExited:SignalTrigger<Void -> Void>;

    var actionToStateMap:StringMap<IState>;

    public function new(name:String)
    {
        this.name = name;

        initSignals();
        initMap();
    }

    inline function initSignals()
    {
        onStateEntered = _onStateEntered = Signal.trigger();
        onStateExited = _onStateExited = Signal.trigger();
    }

    inline function initMap()
    {
        actionToStateMap = new StringMap<IState>();
    }

    public function mapActionToState(action:String, state:IState):Void
    {
        if (action == null || state == null) return;
        actionToStateMap.set(action, state);
    }

    public function transitionOut(action:String):Bool
    {
        if (!actionToStateMap.exists(action)) return false;

        isActive = false;

        _onStateExited.trigger(null);

        if (transitionOutCallback != null) transitionOutCallback();

        return true;
    }

    public function transitioningTo(action:String):IState
    {
        var goingToState = actionToStateMap.get(action);
        return goingToState;
    }

    public function transitionIn():Bool
    {
        isActive = true;

        _onStateEntered.trigger(null);

        if (transitionInCallback != null) transitionInCallback();

        return true;
    }
}
