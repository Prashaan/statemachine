import massive.munit.TestSuite;

import machine.StateMachineTest;
import state.StateTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(machine.StateMachineTest);
		add(state.StateTest);
	}
}
