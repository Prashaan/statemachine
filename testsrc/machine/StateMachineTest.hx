package machine;

import massive.munit.Assert;


class StateMachineTest
{
    var statemachine:StateMachine = null;

    static var transitionInto:Bool = false;
    static var transitioningOut:Bool = false;

    var sceneNames:Array<String> = null;
    var actions:Array<String> = null;

    static var customCallback:Bool = false;

    public function new()
    {
    }

    @BeforeClass
    public function beforeClass()
    {
    }

    @AfterClass
    public function afterClass()
    {
    }

    @Before
    public function setup()
    {
        sceneNames = ["scene1", "scene2", "scene3"];
        actions = ["action1", "action2", "action3"];

        statemachine = new StateMachine();

        transitioningOut = false;
        transitionInto = false;
        customCallback = false;

        setupStatesAndActions();
    }

    function setupStatesAndActions()
    {
        statemachine.createState(sceneNames[0], transitionIntoStateCallback, transitioningOutStateCallback)
        .mapActionToState(actions[0], sceneNames[1]);

        statemachine.createState(sceneNames[1])
        .mapActionToState(actions[1], sceneNames[2])
        .attachCallbacks(attachCallbackForTransitionInto);

        statemachine.createState(sceneNames[2])
        .mapActionToState(actions[2], sceneNames[0]);
    }

    @After
    public function tearDown()
    {
    }

    @Test
    public function testConstruction()
    {
        Assert.isNotNull(statemachine);
    }

    @Test
    public function testInitialSceneCreation()
    {
        Assert.areEqual(statemachine.currentStateName, sceneNames[0]);
    }

    @Test
    public function testDoAction()
    {
        Assert.areEqual(statemachine.currentStateName, sceneNames[0]);

        statemachine.doAction(actions[0]);

        Assert.areEqual(statemachine.currentStateName, sceneNames[1]);

        statemachine.doAction(actions[1]);

        Assert.areEqual(statemachine.currentStateName, sceneNames[2]);

        statemachine.doAction(actions[2]);

        Assert.areEqual(statemachine.currentStateName, sceneNames[0]);
    }

    @Test
    public function testInitialCallbacks()
    {
        Assert.isTrue(transitionInto);
        Assert.isFalse(transitioningOut);

        statemachine.doAction(actions[0]);

        Assert.isTrue(transitioningOut);
        Assert.isFalse(transitionInto);
    }

    @Test
    public function testAttachCallbacks()
    {
        Assert.isFalse(customCallback);
        statemachine.doAction(actions[0]);
        Assert.isTrue(customCallback);
    }

    static function transitionIntoStateCallback()
    {
        transitionInto = true;
        transitioningOut = false;
    }

    static function transitioningOutStateCallback()
    {
        transitionInto = false;
        transitioningOut = true;
    }

    static function attachCallbackForTransitionInto()
    {
        customCallback = true;
    }
}