package state;

import massive.munit.Assert;
import state.IState;
using mockatoo.Mockatoo;

class StateTest
{
    var state:State = null;
    var name:String = null;

    var possibleTransitions:Array<IState> = null;
    var actions:Array<String> = null;

    static var stateEntered:Bool = false;
    static var stateExited:Bool = false;

    public function new()
    {
    }

    @Before
    public function setup()
    {
        name = "mockName";

        possibleTransitions = [createState("state1"), createState("state2"), createState("state3")];
        actions = ["action1", "action2", "action3"];

        state = createState(name);

        stateEntered = false;
        stateExited = false;

        state.transitionOutCallback = stateExitedCallback;
        state.transitionInCallback = stateEnteredCallback;
    }

    @After
    public function tearDown()
    {
    }

    @Test
    public function testConstruction()
    {
        Assert.isNotNull(state);
        Assert.areEqual(state.name, name);

        Assert.isNotNull(state.onStateExited);
        Assert.isNotNull(state.onStateExited);

        Assert.isNull(state.previousState);
    }

    @Test
    public function testMapActionToState()
    {
        performMappingOfActionsToStates(state, actions, possibleTransitions);

        var mappedIndex = 0;
        for (action in actions)
        {
            Assert.areEqual(state.transitioningTo(action), possibleTransitions[mappedIndex++]);
        }
    }

    @Test
    public function testTransitionIntoState()
    {
        Assert.isFalse(state.isActive);
        Assert.isFalse(stateEntered);

        state.transitionIn();

        Assert.isTrue(state.isActive);
        Assert.isTrue(stateEntered);
    }

    @Test
    public function testTransitionOut()
    {
        performMappingOfActionsToStates(state, actions, possibleTransitions);

        state.transitionIn();

        Assert.isTrue(state.isActive);
        Assert.isTrue(stateEntered);

        state.transitionOut("action1");

        Assert.isFalse(state.isActive);
        Assert.isTrue(stateExited);
    }

    static function performMappingOfActionsToStates(state:IState, actions:Array<String>, possibleTransitions:Array<IState>)
    {
        var mappedIndex:Int = 0;
        for (stateTransitions in possibleTransitions)
        {
            state.mapActionToState(actions[mappedIndex++], stateTransitions);
        }
    }

    static function stateEnteredCallback()
    {
        stateEntered = true;
    }

    static function stateExitedCallback()
    {
        stateExited = true;
    }

    static function createState(name:String):State
    {
        return new State(name);
    }
}

